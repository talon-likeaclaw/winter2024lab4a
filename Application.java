public class Application {
	
	public static void main(String[] args) {
 		// Create Talon instance
		Student talon = new Student(28, "Dawson College");
		System.out.println("Talon:");
		System.out.println(talon.getAge());
		talon.setAverageGrade(96.54);
		System.out.println(talon.getAge());
		System.out.println(talon.getSchoolName());
		System.out.println(talon.getAverageGrade());
		talon.honourRole();
		talon.onTime();
		
		// Create Matt instance
		Student matt = new Student(31, "Concordia");
		System.out.println("Matt:");
		matt.setAverageGrade(94.13);
		System.out.println(matt.getAge());
		System.out.println(matt.getSchoolName());
		System.out.println(matt.getAverageGrade());
		matt.honourRole(); 
		
		// Create student array
		Student[] section4 = new Student[3];
		section4[0] = talon;
		section4[1] = matt;
		
		// Create new student instance
		section4[2] = new Student(18, "Dawson College");
		section4[2].setAverageGrade(84.65);
		
		System.out.println(section4[2].getAge());
		System.out.println(section4[2].getSchoolName());
		System.out.println(section4[2].getAverageGrade());
		System.out.println(section4[0].amountLearnt);
		System.out.println(section4[1].amountLearnt);
		System.out.println(section4[2].amountLearnt);
		section4[2].study(15);
		System.out.println(section4[0].amountLearnt);
		System.out.println(section4[1].amountLearnt);
		System.out.println(section4[2].amountLearnt);

		Student buddy = new Student(23, "McGill");
		buddy.setAverageGrade(99.9);
		System.out.println(buddy.getAge());
		System.out.println(buddy.getSchoolName());
		System.out.println(buddy.getAverageGrade());
		System.out.println(buddy.amountLearnt);
	}
	
}