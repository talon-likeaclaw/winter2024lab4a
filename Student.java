public class Student {
	// Student fields
	private int age;
	private String schoolName;
	private double averageGrade;
	public int amountLearnt;
	
	// Constructor Method
	public Student(int age, String schoolName) {
		this.age = age;
		this.schoolName = schoolName;
		this.averageGrade = 80.0;
		this.amountLearnt = 0;
	}
	
	// Setter Methods
	public void setAverageGrade(double newAverageGrade) {
		this.averageGrade = newAverageGrade;
	}
	
	// Getter Methods
	public int getAge() {
		return this.age;
	}
	
	public String getSchoolName() {
		return this.schoolName;
	}
	
	public double getAverageGrade() {
		return this.averageGrade;
	}
	
	// Instance methods
	public void honourRole() {
		if (this.averageGrade > 95.00) {
			System.out.println("Honour Role");
		} else {
			System.out.println("Not Honour Role");
		}
		
	}
	
	public void onTime() {
		System.out.println("Student was on time");
	}
	
	public void study(int amountStudied) {
		this.amountLearnt = this.amountLearnt + amountStudied;
	}
}